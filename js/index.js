var board = [0, 1, 2, 3, 4, 5, 6, 7, 8];
var roundTurn = 0;
var humanColor = "#ffce00";
var aiColor = "#333";
var human = "O";
var ai = "X";

$(window).bind("load", function () {
    $('.loading').fadeOut();
    $("td").click(function () {
        boxClick(this, human, humanColor);
    });
});

//Action upon tileClick
function boxClick(element, player, color) {
    if (board[element.id] != "O" && board[element.id] != "X") {
        roundTurn++;
        $(element).css("background-color", color);
        $(element).text("X");
        board[element.id] = player;
        console.log(board);
        if (checkWinner(board, player)) {
            setTimeout(function () {
                swal({
                    title: "WOOW...You Win!",
                    type: "success",
                    confirmButtonColor: "rgba(165, 220, 134, 0.2);",
                    confirmButtonText: "Try Again",
                    closeOnConfirm: false
                });
                resetBoard();
            }, 500);
            return;
        } else if (roundTurn > 8) {
            setTimeout(function () {
                swal({
                    title: "DRAW!",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Try Again",
                    closeOnConfirm: false
                });
                resetBoard();
            }, 500);
            return;
        } else {
            roundTurn++;
            var index = recurseMinimax(board, ai).index;
            var selector = "#" + index;
            $(selector).css("background-color", aiColor);
            $(selector).text("O");
            board[index] = ai;
            console.log(board);
            console.log(index);
            if (checkWinner(board, ai)) {
                setTimeout(function () {
                    swal({
                        title: "Ooops, You Lost!",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Try Again",
                        closeOnConfirm: false
                    });
                    resetBoard();
                }, 500);
                return;
            } else if (roundTurn === 0) {
                setTimeout(function () {
                    swal({
                        title: "DRAW!",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Try Again",
                        closeOnConfirm: false
                    });
                    resetBoard();
                }, 500);
                return;
            }
        }
    }
}

function resetBoard() {
    roundTurn = 0;
    board = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    $("td").css("background-color", "transparent");
    $("td").text("");
}


// returns list of empty tiles on the board
function emptyTiles(board) {
    return board.filter(s => s != "O" && s != "X");
}

// Winning Rules
function checkWinner(board, player) {
    if (
        (board[0] == player && board[1] == player && board[2] == player) ||
        (board[3] == player && board[4] == player && board[5] == player) ||
        (board[6] == player && board[7] == player && board[8] == player) ||
        (board[0] == player && board[3] == player && board[6] == player) ||
        (board[1] == player && board[4] == player && board[7] == player) ||
        (board[2] == player && board[5] == player && board[8] == player) ||
        (board[0] == player && board[4] == player && board[8] == player) ||
        (board[2] == player && board[4] == player && board[6] == player)
    ) {
        return true;
    } else {
        return false;
    }
}

// The main function
function recurseMinimax(nboard, player) {
    var emptySpots = emptyTiles(nboard);
    if (checkWinner(nboard, human)) {
        return {
            score: -1
        };
    } else if (checkWinner(nboard, ai)) {
        return {
            score: 1
        };
    } else if (emptySpots.length === 0) {
        return {
            score: 0
        };
    }
    // array contains the scores from each of the empty spots 
    var advanceMoves = [];
    // loop through empty spots
    for (var i = 0; i < emptySpots.length; i++) {
        //object of move’s index and score
        var noviceMove = {};
        noviceMove.index = nboard[emptySpots[i]];
        // set the empty spot to the current player
        nboard[emptySpots[i]] = player;
        /*collect the score resulted from calling minimax 
          on the opponent of the current player*/
        if (player == ai) {
            var result = recurseMinimax(nboard, human);
            noviceMove.score = result.score;
        } else {
            var result = recurseMinimax(nboard, ai);
            noviceMove.score = result.score;
        }
        // reset the spot to empty
        nboard[emptySpots[i]] = noviceMove.index;
        // push the object to the array
        advanceMoves.push(noviceMove);
    }
    // if ai turn choose the highest score
    var bestMove;
    if (player === ai) {
        var bestScore = -10000;
        for (var i = 0; i < advanceMoves.length; i++) {
            if (advanceMoves[i].score > bestScore) {
                bestScore = advanceMoves[i].score;
                bestMove = i;
            }
        }
    } else {
        // else choose the lowest score
        var bestScore = 10000;
        for (var i = 0; i < advanceMoves.length; i++) {
            if (advanceMoves[i].score < bestScore) {
                bestScore = advanceMoves[i].score;
                bestMove = i;
            }
        }
    }
    // return the chosen best move from the array
    return advanceMoves[bestMove];
}